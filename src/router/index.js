import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'

import Login from '../views/Login.vue'
Vue.use(VueRouter)

const routes = [

  {
    path: "/",
    name: "login",
    component: Login,
  },
  {
    path: '/home',
    name: 'home',
    component: HomeView,
    redirect: "/home/UserList",
    children: [
      {
        path: '/home/SeatManage',
        component: () => import("@/views/SeatManage/SeatManage.vue"),

      }, {
        path: '/home/SeatOrder',
        component: () => import("@/views/SeatManage/SeatOrder.vue"),
      },
      //卡券管理
      {
        path:'/home/CardMange',
        component:()=>import("@/views/CardManage/CardMange.vue")
      },
      //用户购买记录
      {
        path:'/home/PurchaseRecord',
        component:()=>import("@/views/CardManage/PurchaseRecord.vue")
      },
      {
        path:'/home/yysj',
        name:'yysj',
        component:()=>import('@/views/SetDcom/Yysj.vue')
      },
      {
        path:'/home/yuyuetime',
        name:'yuyuetime',
        component:()=>import('@/views/SetDcom/YuyueView.vue')
      },
      {
        path:'/home/dhmgl',
        name:'dhmgl',
        component:()=>import('@/views/SetDcom/DhmglView')
      },

      // 座位管理
      {

        path:'/home/dhmgl/adddhm',
        name:'adddhm',
        component:()=>import('@/views/SetDcom/AddDhm.vue')
      },
      {
        path:'/home/dmgl',
        name:'dmgl',
        component:()=>import('@/views/GateManagement/GateManagement.vue')
      },
      {
        path:'/home/czgl',
        name:'czgl',
        component:()=>import('@/views/RechargeManagement/RechargeManagement.vue')
      },
      {
        path:'/home/czgl/jgpz',
        name:'jgpz',
        component:()=>import('@/views/RechargeManagement/PriceAllocation.vue')
      },
      {
        path: '/home/OrderManage',
        component: () => import('@/views/OrderManage/OrderManage')
      },
      // 兑换时长管理
      {
        path: '/home/ConvertibleTimeManage',
        component: () => import('@/views/ConvertibleTimeManage/ConvertibleTimeManage')
      },
      // 商家电话时长
      {
        path: '/home/BusinessPhoneTime',
        component: () => import('@/views/BusinessPhoneTime/BusinessPhoneTime')
      },
      // 区域管理
      {
        path: '/home/AreaManage',
        component: () => import('@/views/AreaManage/AreaManage')
      },
      //首页幻灯管理
      {
        path:'/home/HomeSlideManagement',
        component:()=>import('@/views/HomeSlideManagement/HomeSlideManagement.vue')
      },
      //停业时间配置
      {
        path:'/home/ClosingTimeConfiguration',
        component:()=>import('@/views/ClosingTimeConfiguration/ClosingTimeConfiguration.vue')
      },
      //兑换码审核列表
      {
        path:'/home/ExchangeCodeReviewList',
        component:()=>import('@/views/ExchangeCodeReviewList/ExchangeCodeReviewList.vue')
      },
      //用户列表
      {
        path:'/home/UserList',
        component:()=>import('@/views/UserList/UserList')
      },
      {
        path:'/home/UserCard',
        component:()=>import('@/views/UserList/UserCard')
      },
      {
        path:'/home/ClockRecords',
        component:()=>import('@/views/UserList/ClockRecords')
      }

      


    ]
  }

]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
  })

export default router
