import axios from "axios";
axios.defaults.baseURL = "http://47.109.85.44:8080";

// http://118.31.58.45:8080/maintenance/swagger-ui.html
//  添加请求拦截器
axios.interceptors.request.use(
  function (config) {
    // config就是请求的配置
    // Do something before request is sent 在发送请求之前做一些事情
    // 1.从缓存里面取token
    let token = sessionStorage.getItem("token");
    // 2.如果有token就在请求头里设置token
    // config.headers.token = token 请求头里面的token内容  
    //若token存在就让请求头内的token等于我们的token
    token && (config.headers.token = token);
    return config;
  },
  function (error) {
    // 处理请求错误
    return Promise.reject(error);
  }
);

// 添加响应拦截器
axios.interceptors.response.use(
  function (response) {
    // 在2xx范围内的任何状态代码都会触发此函数
    // 使用响应数据执行某些操作
    return response;
  },
  function (error) {
    switch (error.response.status) {
      case 401: //token有问题
        sessionStorage.clear();
        alert("重新登陆");
        window.location.href = "/";
        break;
      case 10086: //根据业务，在这里配置
        break;
    }
    return Promise.reject(error);
  }
); 

export default axios;
